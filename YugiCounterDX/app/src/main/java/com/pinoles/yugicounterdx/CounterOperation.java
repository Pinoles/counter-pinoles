package com.pinoles.yugicounterdx;

import android.content.Context;
import android.media.MediaPlayer;
import android.widget.Toast;

import java.util.Random;

public class CounterOperation {
    CounterOperation(){

    }

    public int damage(int dmgButton,int dmgTxtDamage, boolean swtDamage,int Life){
        int dmg=  dmgTxtDamage;
        if(!swtDamage) {
            dmg -= dmgButton;
            if(dmg <= Life*-1)
                dmg = Life*-1;
        }else {
            dmg += dmgButton;
        }
        return dmg;
    }

    public String Flip(){
        if(new Random().nextBoolean()){
            return "Heads";
        }else {
            return "Tails";
        }
    }

    public void startSound(Context context, boolean checked){
        if(!checked)
            return;

        MediaPlayer lpSound;
        if(new Random().nextFloat()<0.2){
            lpSound = MediaPlayer.create(context,R.raw.yugi);
        }else{
            lpSound = MediaPlayer.create(context,R.raw.lp_sound);
        }
        lpSound.start();
    }
}
