package com.pinoles.yugicounterdx;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.Stack;

public class FourPlayersActivity extends AppCompatActivity {
    View view1P;

    ImageButton imgBtnSetting,imgBtnDado,imgBtnFlip,btnBoot1P,btnBoot2P,btnBoot3P,btnBoot4P;
    Button btn50Dmg1PF,btn100Dmg1PF,btn500Dmg1PF,btn1000Dmg1PF,btnOk1PF,btnClear1PF
            ,btn50Dmg2PF,btn100Dmg2PF,btn500Dmg2PF,btn1000Dmg2PF,btnDivide1PF,btnDivide2PF,btnOk2PF,btnClear2PF,
            btn50Dmg3PF,btn100Dmg3PF,btn500Dmg3PF,btn1000Dmg3PF,btnOk3PF,btnClear3PF
            ,btn50Dmg4PF,btn100Dmg4PF,btn500Dmg4PF,btn1000Dmg4PF,btnDivide3PF,btnDivide4PF,btnOk4PF,btnClear4PF
            ,btnVolverDmg1P,btnVolverDmg2P,btnVolverDmg3P,btnVolverDmg4P,btnHistory;
    TextView txtLifeCounter1PF,txtLifeCounter2PF,txtDamage1PF,txtDamage2PF,
            txtLifeCounter3PF,txtLifeCounter4PF,txtDamage3PF,txtDamage4PF;
    Switch swtDamage1PF,swtDamage2PF,swtDamage3PF,swtDamage4PF;
    CounterOperation mOperations = new CounterOperation();

    int Damage1pF,Damage2pF,sLife1pF,sLife2pF,Damage3pF,Damage4pF,sLife3pF,sLife4pF;
    String Life,History="";
    boolean checked,boot1P = false,boot2P = false,boot3P = false, boot4P = false;
    MediaPlayer lpSound,Musica;
    Stack<Integer> stack1P = new Stack<>();
    Stack<Integer> stack2P = new Stack<>();
    Stack<Integer> stack3P = new Stack<>();
    Stack<Integer> stack4P = new Stack<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_four_players);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Damage1pF  = 0;
        Damage2pF = 0;
        Damage3pF  = 0;
        Damage4pF = 0;

        lpSound = MediaPlayer.create(this, R.raw.lp_sound);

        btn50Dmg1PF = (Button) findViewById(R.id.btn50Dmg1PF);
        btn100Dmg1PF = (Button) findViewById(R.id.btn100Dmg1PF);
        btn500Dmg1PF = (Button) findViewById(R.id.btn500Dmg1PF);
        btn1000Dmg1PF = (Button) findViewById(R.id.btn1000Dmg1PF);
        btnDivide1PF = (Button)findViewById(R.id.btnDivide1PF);
        btn50Dmg2PF = (Button) findViewById(R.id.btn50Dmg2PF);
        btn100Dmg2PF = (Button) findViewById(R.id.btn100Dmg2PF);
        btn500Dmg2PF = (Button) findViewById(R.id.btn500Dmg2PF);
        btn1000Dmg2PF = (Button) findViewById(R.id.btn1000Dmg2PF);
        btnDivide2PF = (Button)findViewById(R.id.btnDivide2PF);
        btn50Dmg3PF = (Button) findViewById(R.id.btn50Dmg3PF);
        btn100Dmg3PF = (Button) findViewById(R.id.btn100Dmg3PF);
        btn500Dmg3PF = (Button) findViewById(R.id.btn500Dmg3PF);
        btn1000Dmg3PF = (Button) findViewById(R.id.btn1000Dmg3PF);
        btnDivide3PF = (Button)findViewById(R.id.btnDivide3PF);
        btn50Dmg4PF = (Button) findViewById(R.id.btn50Dmg4PF);
        btn100Dmg4PF = (Button) findViewById(R.id.btn100Dmg4PF);
        btn500Dmg4PF = (Button) findViewById(R.id.btn500Dmg4PF);
        btn1000Dmg4PF = (Button) findViewById(R.id.btn1000Dmg4PF);
        btnDivide4PF = (Button)findViewById(R.id.btnDivide4PF);
        btnOk1PF = (Button)findViewById(R.id.btnOk1PF);
        btnOk2PF = (Button)findViewById(R.id.btnOk2PF);
        btnOk3PF = (Button)findViewById(R.id.btnOk3PF);
        btnOk4PF = (Button)findViewById(R.id.btnOk4PF);
        btnClear1PF = (Button)findViewById(R.id.btnClear1PF);
        btnClear2PF = (Button)findViewById(R.id.btnClear2PF);
        btnClear3PF = (Button)findViewById(R.id.btnClear3PF);
        btnClear4PF = (Button)findViewById(R.id.btnClear4PF);
        btnVolverDmg1P = (Button)findViewById(R.id.btnBackdmg1P);
        btnVolverDmg2P = (Button)findViewById(R.id.btnBackdmg2P);
        btnVolverDmg3P = (Button)findViewById(R.id.btnBackdmg3P);
        btnVolverDmg4P = (Button)findViewById(R.id.btnBackdmg4P);
        btnHistory = (Button)findViewById(R.id.btnHistory);
        imgBtnDado = (ImageButton) findViewById(R.id.imgBtnDado4P);
        imgBtnFlip = (ImageButton) findViewById(R.id.imgBtnFlip4P);
        btnBoot1P = (ImageButton) findViewById(R.id.btnBoot1PF);
        btnBoot2P = (ImageButton) findViewById(R.id.btnBoot2PF);
        btnBoot3P = (ImageButton) findViewById(R.id.btnBoot3PF);
        btnBoot4P = (ImageButton) findViewById(R.id.btnBoot4PF);
        imgBtnSetting = (ImageButton) findViewById(R.id.imgBtnSetting4P);

        txtLifeCounter1PF = (TextView) findViewById(R.id.txtLifeCounter1PF);
        txtLifeCounter2PF = (TextView) findViewById(R.id.txtLifeCounter2PF);
        txtLifeCounter3PF = (TextView) findViewById(R.id.txtLifeCounter3PF);
        txtLifeCounter4PF = (TextView) findViewById(R.id.txtLifeCounter4PF);
        txtDamage1PF = (TextView) findViewById(R.id.txtDamage1PF);
        txtDamage2PF = (TextView) findViewById(R.id.txtDamage2PF);
        txtDamage3PF = (TextView) findViewById(R.id.txtDamage3PF);
        txtDamage4PF = (TextView) findViewById(R.id.txtDamage4PF);

        swtDamage1PF = (Switch) findViewById(R.id.swtDamage1PF);
        swtDamage2PF = (Switch) findViewById(R.id.swtDamage2PF);
        swtDamage3PF = (Switch) findViewById(R.id.swtDamage3PF);
        swtDamage4PF = (Switch) findViewById(R.id.swtDamage4PF);

        view1P =(View)findViewById(R.id.divider1P);

        if (getIntent().getIntExtra("lp", 2) == 1) {
            Life = "4000";
        } else if (getIntent().getIntExtra("lp", 2) == 2) {
            Life = "8000";
        } else {
            Life = "16000";
        }
        txtLifeCounter2PF.setText(Life);
        txtLifeCounter1PF.setText(Life);
        txtLifeCounter4PF.setText(Life);
        txtLifeCounter3PF.setText(Life);
        sLife1pF = Integer.parseInt(Life);
        sLife2pF = sLife1pF;
        sLife4pF = sLife1pF;
        sLife3pF = sLife1pF;


        checked = getIntent().getBooleanExtra("sound",true);

        view1P.setBackgroundResource(getIntent().getIntExtra("images",R.color.colorGray));

        if(getIntent().getBooleanExtra("musica",true)){
            Musica = MediaPlayer.create(FourPlayersActivity.this,R.raw.duel);
            Musica.start();
            Musica.setLooping(true);
        }

        btn50Dmg1PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage1PF(50);
            }
        });

        btn100Dmg1PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage1PF(100);
            }
        });

        btn500Dmg1PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage1PF(500);
            }
        });

        btn1000Dmg1PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage1PF(1000);
            }
        });

        btnDivide1PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!swtDamage1PF.isChecked()){
                    SubirDamage1PF(sLife1pF/2);
                }else{
                    SubirDamage1PF(sLife1pF);
                }
            }
        });

        btn50Dmg2PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage2PF(50);
            }
        });

        btn100Dmg2PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage2PF(100);
            }
        });

        btn500Dmg2PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage2PF(500);
            }
        });

        btn1000Dmg2PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage2PF(1000);
            }
        });

        btnDivide2PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!swtDamage2PF.isChecked()){
                    SubirDamage2PF(sLife2pF/2);
                }else{
                    SubirDamage2PF(sLife2pF);
                }
            }
        });

        btn50Dmg3PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage3PF(50);
            }
        });

        btn100Dmg3PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage3PF(100);
            }
        });

        btn500Dmg3PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage3PF(500);
            }
        });

        btn1000Dmg3PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage3PF(1000);
            }
        });

        btnDivide3PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!swtDamage3PF.isChecked()){
                    SubirDamage3PF(sLife3pF/2);
                }else{
                    SubirDamage3PF(sLife3pF);
                }
            }
        });

        btn50Dmg4PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage4PF(50);
            }
        });

        btn100Dmg4PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage4PF(100);
            }
        });

        btn500Dmg4PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage4PF(500);
            }
        });

        btn1000Dmg4PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage4PF(1000);
            }
        });

        btnDivide4PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!swtDamage4PF.isChecked()){
                    SubirDamage4PF(sLife4pF/2);
                }else{
                    SubirDamage4PF(sLife4pF);
                }
            }
        });

        swtDamage1PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (swtDamage1PF.isChecked()) {
                    btn50Dmg1PF.setBackgroundColor(Color.GREEN);
                    btn100Dmg1PF.setBackgroundColor(Color.GREEN);
                    btn500Dmg1PF.setBackgroundColor(Color.GREEN);
                    btn1000Dmg1PF.setBackgroundColor(Color.GREEN);
                    btnDivide1PF.setBackgroundColor(Color.GREEN);
                    btnDivide1PF.setText("*2");
                } else {
                    btn50Dmg1PF.setBackgroundColor(Color.RED);
                    btn100Dmg1PF.setBackgroundColor(Color.RED);
                    btn500Dmg1PF.setBackgroundColor(Color.RED);
                    btn1000Dmg1PF.setBackgroundColor(Color.RED);
                    btnDivide1PF.setBackgroundColor(Color.RED);
                    btnDivide1PF.setText("/2");
                }
            }
        });

        swtDamage2PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (swtDamage2PF.isChecked()) {
                    btn50Dmg2PF.setBackgroundColor(Color.GREEN);
                    btn100Dmg2PF.setBackgroundColor(Color.GREEN);
                    btn500Dmg2PF.setBackgroundColor(Color.GREEN);
                    btn1000Dmg2PF.setBackgroundColor(Color.GREEN);
                    btnDivide2PF.setBackgroundColor(Color.GREEN);
                    btnDivide2PF.setText("*2");
                } else {
                    btn50Dmg2PF.setBackgroundColor(Color.RED);
                    btn100Dmg2PF.setBackgroundColor(Color.RED);
                    btn500Dmg2PF.setBackgroundColor(Color.RED);
                    btn1000Dmg2PF.setBackgroundColor(Color.RED);
                    btnDivide2PF.setBackgroundColor(Color.RED);
                    btnDivide2PF.setText("/2");
                }
            }
        });

        swtDamage3PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (swtDamage3PF.isChecked()) {
                    btn50Dmg3PF.setBackgroundColor(Color.GREEN);
                    btn100Dmg3PF.setBackgroundColor(Color.GREEN);
                    btn500Dmg3PF.setBackgroundColor(Color.GREEN);
                    btn1000Dmg3PF.setBackgroundColor(Color.GREEN);
                    btnDivide3PF.setBackgroundColor(Color.GREEN);
                    btnDivide3PF.setText("*2");
                } else {
                    btn50Dmg3PF.setBackgroundColor(Color.RED);
                    btn100Dmg3PF.setBackgroundColor(Color.RED);
                    btn500Dmg3PF.setBackgroundColor(Color.RED);
                    btn1000Dmg3PF.setBackgroundColor(Color.RED);
                    btnDivide3PF.setBackgroundColor(Color.RED);
                    btnDivide3PF.setText("/2");
                }
            }
        });

        swtDamage4PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (swtDamage4PF.isChecked()) {
                    btn50Dmg4PF.setBackgroundColor(Color.GREEN);
                    btn100Dmg4PF.setBackgroundColor(Color.GREEN);
                    btn500Dmg4PF.setBackgroundColor(Color.GREEN);
                    btn1000Dmg4PF.setBackgroundColor(Color.GREEN);
                    btnDivide4PF.setBackgroundColor(Color.GREEN);
                    btnDivide4PF.setText("*2");
                } else {
                    btn50Dmg4PF.setBackgroundColor(Color.RED);
                    btn100Dmg4PF.setBackgroundColor(Color.RED);
                    btn500Dmg4PF.setBackgroundColor(Color.RED);
                    btn1000Dmg4PF.setBackgroundColor(Color.RED);
                    btnDivide4PF.setBackgroundColor(Color.RED);
                    btnDivide4PF.setText("/2");
                }
            }
        });

        btnOk1PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Damage1pF == 0)
                    return;

                final int RealDamage = sLife1pF;
                final int dmg = Damage1pF;
                stack1P.push(dmg*-1);
                btnVolverDmg1P.setVisibility(View.VISIBLE);
                sLife1pF = RealDamage + dmg;
                if(dmg>0)
                    History += ("Jugador 1: "+txtLifeCounter1PF.getText()+"+"+txtDamage1PF.getText()+"="+String.valueOf(sLife1pF)+"\n");
                else
                    History += ("Jugador 1: "+txtLifeCounter1PF.getText()+txtDamage1PF.getText()+"="+String.valueOf(sLife1pF)+"\n");
                txtDamage1PF.setText("0");
                Damage1pF= 0;
                txtDamage1PF.setVisibility(View.INVISIBLE);
                btnOk1PF.setVisibility(View.INVISIBLE);
                btnClear1PF.setVisibility(View.INVISIBLE);
                mOperations.startSound(FourPlayersActivity.this,checked);

                new CountDownTimer(2500, 250) {
                    public void onTick(long millisUntilFinished) {
                        txtLifeCounter1PF.setText(String.valueOf(new Random().nextInt(8000)));
                    }

                    public void onFinish() {
                        if (sLife1pF == 0) {
                            new AlertDialog.Builder(FourPlayersActivity.this)
                                    .setMessage("Tus puntos de vida han quedado a cero\n" +
                                            "desea reiniciar?")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            txtLifeCounter1PF.setText(Life);
                                            txtLifeCounter2PF.setText(Life);
                                            sLife1pF = Integer.parseInt(Life);
                                            sLife2pF = Integer.parseInt(Life);
                                            txtLifeCounter3PF.setText(Life);
                                            txtLifeCounter4PF.setText(Life);
                                            sLife3pF = Integer.parseInt(Life);
                                            sLife4pF = Integer.parseInt(Life);
                                            stack1P.clear();
                                            stack2P.clear();
                                            stack3P.clear();
                                            stack4P.clear();
                                            btnVolverDmg1P.setVisibility(View.INVISIBLE);
                                            btnVolverDmg2P.setVisibility(View.INVISIBLE);
                                            btnVolverDmg3P.setVisibility(View.INVISIBLE);
                                            btnVolverDmg4P.setVisibility(View.INVISIBLE);
                                            History="";

                                        }
                                    }).setNegativeButton("NO", null).create().show();
                        }
                        txtLifeCounter1PF.setText(String.valueOf(sLife1pF));
                    }
                }.start();
            }
        });

        btnOk2PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Damage2pF == 0)
                    return;

                final int RealDamage = sLife2pF;
                final int dmg = Damage2pF;
                stack2P.push(dmg*-1);
                btnVolverDmg2P.setVisibility(View.VISIBLE);
                sLife2pF = RealDamage + dmg;
                if(dmg>0)
                    History += ("Jugador 2: "+txtLifeCounter2PF.getText()+"+"+txtDamage2PF.getText()+"="+String.valueOf(sLife2pF)+"\n");
                else
                    History += ("Jugador 2: "+txtLifeCounter2PF.getText()+txtDamage2PF.getText()+"="+String.valueOf(sLife2pF)+"\n");
                txtDamage2PF.setText("0");
                Damage2pF= 0;
                txtDamage2PF.setVisibility(View.INVISIBLE);
                btnOk2PF.setVisibility(View.INVISIBLE);
                btnClear2PF.setVisibility(View.INVISIBLE);
                mOperations.startSound(FourPlayersActivity.this,checked);

                new CountDownTimer(2500, 250) {
                    public void onTick(long millisUntilFinished) {
                        txtLifeCounter2PF.setText(String.valueOf(new Random().nextInt(8000)));
                    }

                    public void onFinish() {
                        if (sLife2pF == 0) {
                            new AlertDialog.Builder(FourPlayersActivity.this)
                                    .setMessage("Tus puntos de vida han quedado a cero\n" +
                                            "desea reiniciar?")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            txtLifeCounter1PF.setText(Life);
                                            txtLifeCounter2PF.setText(Life);
                                            sLife1pF = Integer.parseInt(Life);
                                            sLife2pF = Integer.parseInt(Life);
                                            txtLifeCounter3PF.setText(Life);
                                            txtLifeCounter4PF.setText(Life);
                                            sLife3pF = Integer.parseInt(Life);
                                            sLife4pF = Integer.parseInt(Life);
                                            stack1P.clear();
                                            stack2P.clear();
                                            stack3P.clear();
                                            stack4P.clear();
                                            btnVolverDmg1P.setVisibility(View.INVISIBLE);
                                            btnVolverDmg2P.setVisibility(View.INVISIBLE);
                                            btnVolverDmg3P.setVisibility(View.INVISIBLE);
                                            btnVolverDmg4P.setVisibility(View.INVISIBLE);
                                            History="";
                                        }
                                    }).setNegativeButton("NO", null).create().show();
                        }
                        txtLifeCounter2PF.setText(String.valueOf(sLife2pF));
                    }
                }.start();
            }
        });

        btnOk3PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Damage3pF == 0)
                    return;

                final int RealDamage = sLife3pF;
                final int dmg = Damage3pF;
                sLife3pF = RealDamage + dmg;
                stack3P.push(dmg*-1);
                btnVolverDmg3P.setVisibility(View.VISIBLE);
                if(dmg>0)
                    History += ("Jugador 3: "+txtLifeCounter3PF.getText()+"+"+txtDamage3PF.getText()+"="+String.valueOf(sLife3pF)+"\n");
                else
                    History += ("Jugador 3: "+txtLifeCounter3PF.getText()+txtDamage3PF.getText()+"="+String.valueOf(sLife3pF)+"\n");
                txtDamage3PF.setText("0");
                Damage3pF= 0;
                txtDamage3PF.setVisibility(View.INVISIBLE);
                btnOk3PF.setVisibility(View.INVISIBLE);
                btnClear3PF.setVisibility(View.INVISIBLE);
                mOperations.startSound(FourPlayersActivity.this,checked);

                new CountDownTimer(2500, 250) {
                    public void onTick(long millisUntilFinished) {
                        txtLifeCounter3PF.setText(String.valueOf(new Random().nextInt(8000)));
                    }

                    public void onFinish() {
                        if (sLife3pF == 0) {
                            new AlertDialog.Builder(FourPlayersActivity.this)
                                    .setMessage("Tus puntos de vida han quedado a cero\n" +
                                            "desea reiniciar?")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            txtLifeCounter1PF.setText(Life);
                                            txtLifeCounter2PF.setText(Life);
                                            sLife1pF = Integer.parseInt(Life);
                                            sLife2pF = Integer.parseInt(Life);
                                            txtLifeCounter3PF.setText(Life);
                                            txtLifeCounter4PF.setText(Life);
                                            sLife3pF = Integer.parseInt(Life);
                                            sLife4pF = Integer.parseInt(Life);
                                            stack1P.clear();
                                            stack2P.clear();
                                            stack3P.clear();
                                            stack4P.clear();
                                            btnVolverDmg1P.setVisibility(View.INVISIBLE);
                                            btnVolverDmg2P.setVisibility(View.INVISIBLE);
                                            btnVolverDmg3P.setVisibility(View.INVISIBLE);
                                            btnVolverDmg4P.setVisibility(View.INVISIBLE);
                                            History="";
                                        }
                                    }).setNegativeButton("NO", null).create().show();
                        }
                        txtLifeCounter3PF.setText(String.valueOf(sLife3pF));
                    }
                }.start();
            }
        });

        btnOk4PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Damage4pF == 0)
                    return;

                final int RealDamage = sLife4pF;
                final int dmg = Damage4pF;
                stack4P.push(dmg*-1);
                btnVolverDmg4P.setVisibility(View.VISIBLE);
                sLife4pF = RealDamage + dmg;
                if(dmg>0)
                    History += ("Jugador4: "+txtLifeCounter4PF.getText()+"+"+txtDamage4PF.getText()+"="+String.valueOf(sLife4pF)+"\n");
                else
                    History += ("Jugador4: "+txtLifeCounter4PF.getText()+txtDamage4PF.getText()+"="+String.valueOf(sLife4pF)+"\n");
                txtDamage4PF.setText("0");
                Damage4pF= 0;
                txtDamage4PF.setVisibility(View.INVISIBLE);
                btnOk4PF.setVisibility(View.INVISIBLE);
                btnClear4PF.setVisibility(View.INVISIBLE);
                mOperations.startSound(FourPlayersActivity.this,checked);

                new CountDownTimer(2500, 250) {
                    public void onTick(long millisUntilFinished) {
                        txtLifeCounter4PF.setText(String.valueOf(new Random().nextInt(8000)));
                    }

                    public void onFinish() {
                        if (sLife4pF == 0) {
                            new AlertDialog.Builder(FourPlayersActivity.this)
                                    .setMessage("Tus puntos de vida han quedado a cero\n" +
                                            "desea reiniciar?")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            txtLifeCounter1PF.setText(Life);
                                            txtLifeCounter2PF.setText(Life);
                                            sLife1pF = Integer.parseInt(Life);
                                            sLife2pF = Integer.parseInt(Life);
                                            txtLifeCounter3PF.setText(Life);
                                            txtLifeCounter4PF.setText(Life);
                                            sLife3pF = Integer.parseInt(Life);
                                            sLife4pF = Integer.parseInt(Life);
                                            stack1P.clear();
                                            stack2P.clear();
                                            stack3P.clear();
                                            stack4P.clear();
                                            btnVolverDmg1P.setVisibility(View.INVISIBLE);
                                            btnVolverDmg2P.setVisibility(View.INVISIBLE);
                                            btnVolverDmg3P.setVisibility(View.INVISIBLE);
                                            btnVolverDmg4P.setVisibility(View.INVISIBLE);
                                            History="";
                                        }
                                    }).setNegativeButton("NO", null).create().show();
                        }
                        txtLifeCounter4PF.setText(String.valueOf(sLife4pF));
                    }
                }.start();
            }
        });

        btnVolverDmg1P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!stack1P.empty()){
                    int dmg = stack1P.pop();
                    sLife1pF = sLife1pF + dmg;
                    if(dmg>0)
                        History += ("Jugador 1: "+txtLifeCounter1PF.getText()+"+"+String.valueOf(dmg)+"="+String.valueOf(sLife1pF)+"\n");
                    else
                        History += ("Jugador 1: "+txtLifeCounter1PF.getText()+String.valueOf(dmg)+"="+String.valueOf(sLife1pF)+"\n");
                    txtLifeCounter1PF.setText(String.valueOf(sLife1pF));
                    if(stack1P.empty()){
                        btnVolverDmg1P.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

        btnVolverDmg2P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!stack2P.empty()){
                    int dmg = stack2P.pop();
                    sLife2pF = sLife2pF + dmg;
                    if(dmg>0)
                        History += ("Jugador 2: "+txtLifeCounter2PF.getText()+"+"+String.valueOf(dmg)+"="+String.valueOf(sLife2pF)+"\n");
                    else
                        History += ("Jugador 2: "+txtLifeCounter2PF.getText()+String.valueOf(dmg)+"="+String.valueOf(sLife2pF)+"\n");
                    txtLifeCounter2PF.setText(String.valueOf(sLife2pF));
                    if(stack2P.empty()){
                        btnVolverDmg2P.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

        btnVolverDmg3P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!stack3P.empty()){
                    int dmg = stack3P.pop();
                    sLife3pF = sLife3pF + dmg;
                    if(dmg>0)
                        History += ("Jugador 3: "+txtLifeCounter3PF.getText()+"+"+String.valueOf(dmg)+"="+String.valueOf(sLife3pF)+"\n");
                    else
                        History += ("Jugador 3: "+txtLifeCounter3PF.getText()+String.valueOf(dmg)+"="+String.valueOf(sLife3pF)+"\n");
                    txtLifeCounter3PF.setText(String.valueOf(sLife3pF));
                    if(stack3P.empty()){
                        btnVolverDmg3P.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

        btnVolverDmg4P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!stack4P.empty()){
                    int dmg = stack4P.pop();
                    sLife4pF = sLife4pF + dmg;
                    if(dmg>0)
                        History += ("Jugador 4: "+txtLifeCounter4PF.getText()+"+"+String.valueOf(dmg)+"="+String.valueOf(sLife4pF)+"\n");
                    else
                        History += ("Jugador 4: "+txtLifeCounter4PF.getText()+String.valueOf(dmg)+"="+String.valueOf(sLife4pF)+"\n");
                    txtLifeCounter4PF.setText(String.valueOf(sLife4pF));
                    if(stack4P.empty()){
                        btnVolverDmg4P.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

        btnClear1PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnClear1PF.setVisibility(View.INVISIBLE);
                btnOk1PF.setVisibility(View.INVISIBLE);
                txtDamage1PF.setText("0");
                Damage1pF = 0;
                txtDamage1PF.setVisibility(View.INVISIBLE);
            }
        });

        btnClear2PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnClear2PF.setVisibility(View.INVISIBLE);
                btnOk2PF.setVisibility(View.INVISIBLE);
                txtDamage2PF.setText("0");
                Damage2pF = 0;
                txtDamage2PF.setVisibility(View.INVISIBLE);
            }
        });

        btnClear3PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnClear3PF.setVisibility(View.INVISIBLE);
                btnOk3PF.setVisibility(View.INVISIBLE);
                txtDamage3PF.setText("0");
                Damage3pF = 0;
                txtDamage3PF.setVisibility(View.INVISIBLE);
            }
        });

        btnClear4PF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnClear4PF.setVisibility(View.INVISIBLE);
                btnOk4PF.setVisibility(View.INVISIBLE);
                txtDamage4PF.setText("0");
                Damage4pF = 0;
                txtDamage4PF.setVisibility(View.INVISIBLE);
            }
        });

        btnBoot1P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtLifeCounter1PF.setText(Life);
                sLife1pF = Integer.parseInt(Life);
                stack1P.clear();
                btnVolverDmg1P.setVisibility(View.INVISIBLE);
                boot1P = true;
                if(boot1P && boot2P && boot3P && boot4P){
                    History="";
                    boot1P = false;
                    boot2P=false;
                    boot3P = false;
                    boot4P=false;
                }else {
                    History += "Jugador 1: Reinicio contador a " + Life +"\n";
                }
            }
        });

        btnBoot2P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtLifeCounter2PF.setText(Life);
                sLife2pF = Integer.parseInt(Life);
                stack2P.clear();
                btnVolverDmg2P.setVisibility(View.INVISIBLE);
                boot2P = true;
                if(boot1P && boot2P && boot3P && boot4P){
                    History="";
                    boot1P = false;
                    boot2P=false;
                    boot3P = false;
                    boot4P=false;
                }else {
                    History += "Jugador 2: Reinicio contador a " + Life +"\n";
                }
            }
        });

        btnBoot3P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtLifeCounter3PF.setText(Life);
                sLife3pF = Integer.parseInt(Life);
                stack3P.clear();
                btnVolverDmg3P.setVisibility(View.INVISIBLE);
                boot3P = true;
                if(boot1P && boot2P && boot3P && boot4P){
                    History="";
                    boot1P = false;
                    boot2P=false;
                    boot3P = false;
                    boot4P=false;
                }else {
                    History += "Jugador 3: Reinicio contador a " + Life +"\n";
                }
            }
        });

        btnBoot4P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtLifeCounter4PF.setText(Life);
                sLife4pF = Integer.parseInt(Life);
                stack4P.clear();
                btnVolverDmg4P.setVisibility(View.INVISIBLE);
                boot4P = true;
                if(boot1P && boot2P && boot3P && boot4P){
                    History="";
                    boot1P = false;
                    boot2P=false;
                    boot3P = false;
                    boot4P=false;
                }else {
                    History += "Jugador 4: Reinicio contador a " + Life +"\n";
                }
            }
        });

        imgBtnDado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(FourPlayersActivity.this,
                        "El resultado del dado es " + String.valueOf(new Random().nextInt(6) + 1),
                        Toast.LENGTH_SHORT).show();
            }
        });

        imgBtnFlip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String moneda = mOperations.Flip();
                Toast.makeText(FourPlayersActivity.this,
                        ("El resultado de la moneda es " + moneda),
                        Toast.LENGTH_SHORT).show();
            }
        });

        imgBtnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Musica != null)
                    Musica.stop();
                Intent i = new Intent(FourPlayersActivity.this, config.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("inicio", true);
                startActivity(i);
                finish();
            }
        });

        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(FourPlayersActivity.this)
                        .setMessage(History)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {}
                        }).create().show();
            }
        });
    }



    private  void SubirDamage1PF(int btnDamage){
        int Life = sLife1pF;
        Damage1pF = mOperations.damage(btnDamage,Damage1pF,swtDamage1PF.isChecked(),Life);
        if(!swtDamage1PF.isChecked()) {
            txtDamage1PF.setText(String.valueOf(Damage1pF));
            txtDamage1PF.setTextColor(Color.RED);
        }else{
            txtDamage1PF.setText(String.valueOf(Damage1pF));
            if(Damage1pF ==0 )
                txtDamage1PF.setTextColor(Color.WHITE);
            else if(Damage1pF >0){
                txtDamage1PF.setTextColor((Color.GREEN));
            }
        }
        txtDamage1PF.setVisibility(View.VISIBLE);
        btnClear1PF.setVisibility(View.VISIBLE);
        btnOk1PF.setVisibility(View.VISIBLE);
    }

    private  void SubirDamage2PF(int btnDamage){
        int Life = sLife2pF;
        Damage2pF= mOperations.damage(btnDamage,Damage2pF,swtDamage2PF.isChecked(),Life);
        if(!swtDamage2PF.isChecked()) {
            txtDamage2PF.setText(String.valueOf(Damage2pF));
            txtDamage2PF.setTextColor(Color.RED);
        }else{
            txtDamage2PF.setText(String.valueOf(Damage2pF));
            if(Damage2pF==0)
                txtDamage2PF.setTextColor(Color.WHITE);
            else if(Damage2pF>0){
                txtDamage2PF.setTextColor(Color.GREEN);
            }
        }
        txtDamage2PF.setVisibility(View.VISIBLE);
        btnClear2PF.setVisibility(View.VISIBLE);
        btnOk2PF.setVisibility(View.VISIBLE);
    }

    private  void SubirDamage3PF(int btnDamage){
        int Life = sLife3pF;
        Damage3pF = mOperations.damage(btnDamage,Damage3pF,swtDamage3PF.isChecked(),Life);
        if(!swtDamage3PF.isChecked()) {
            txtDamage3PF.setText(String.valueOf(Damage3pF));
            txtDamage3PF.setTextColor(Color.RED);
        }else{
            txtDamage3PF.setText(String.valueOf(Damage3pF));
            if(Damage3pF ==0 )
                txtDamage1PF.setTextColor(Color.WHITE);
            else if(Damage3pF >0){
                txtDamage3PF.setTextColor((Color.GREEN));
            }
        }
        txtDamage3PF.setVisibility(View.VISIBLE);
        btnClear3PF.setVisibility(View.VISIBLE);
        btnOk3PF.setVisibility(View.VISIBLE);
    }

    private  void SubirDamage4PF(int btnDamage){
        int Life = sLife4pF;
        Damage4pF= mOperations.damage(btnDamage,Damage4pF,swtDamage4PF.isChecked(),Life);
        if(!swtDamage4PF.isChecked()) {
            txtDamage4PF.setText(String.valueOf(Damage4pF));
            txtDamage4PF.setTextColor(Color.RED);
        }else{
            txtDamage4PF.setText(String.valueOf(Damage4pF));
            if(Damage4pF==0)
                txtDamage4PF.setTextColor(Color.WHITE);
            else if(Damage4pF>0){
                txtDamage4PF.setTextColor(Color.GREEN);
            }
        }
        txtDamage4PF.setVisibility(View.VISIBLE);
        btnClear4PF.setVisibility(View.VISIBLE);
        btnOk4PF.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finishAffinity();
        System.exit(0);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if(Musica != null)
            Musica.pause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Musica != null)
            Musica.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finishAffinity();

    }
}
