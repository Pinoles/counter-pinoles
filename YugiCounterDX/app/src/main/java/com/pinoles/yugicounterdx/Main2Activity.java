package com.pinoles.yugicounterdx;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.Stack;

public class Main2Activity extends AppCompatActivity {

    View view1P;

    ImageButton imgBtnSetting,imgBtnDado,imgBtnFlip,btnBoot1P,btnBoot2P;
    Button btn50Dmg1P,btn100Dmg1P,btn500Dmg1P,btn1000Dmg1P,btnOk1P,btnClear1P
            ,btn50Dmg2P,btn100Dmg2P,btn500Dmg2P,btn1000Dmg2P,btnDivide1P,btnDivide2P,btnOk2P,btnClear2P
            ,btnVolverdmg1P,btnVolverdmg2P,btnHistory;
    TextView txtLifeCounter1P,txtLifeCounter2P,txtDamage1P,txtDamage2P;
    Switch swtDamage1P,swtDamage2P;
    CounterOperation mOperations = new CounterOperation();

    int Damage1p,Damage2p,sLife1p,sLife2p;
    String Life,History="";
    boolean checked,boot1P = false,boot2P=false;
    Stack<Integer> stack1P = new Stack<Integer>();
    Stack<Integer> stack2P = new Stack<Integer>();
    MediaPlayer lpSound,Musica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main2);
            Damage1p  = 0;
            Damage2p = 0;

            lpSound = MediaPlayer.create(this, R.raw.lp_sound);

            btn50Dmg1P = (Button) findViewById(R.id.btn50Dmg1P);
            btn100Dmg1P = (Button) findViewById(R.id.btn100Dmg1P);
            btn500Dmg1P = (Button) findViewById(R.id.btn500Dmg1P);
            btn1000Dmg1P = (Button) findViewById(R.id.btn1000Dmg1P);
            btnDivide1P = (Button)findViewById(R.id.btnDivide1P);
            btn50Dmg2P = (Button) findViewById(R.id.btn50Dmg2P);
            btn100Dmg2P = (Button) findViewById(R.id.btn100Dmg2P);
            btn500Dmg2P = (Button) findViewById(R.id.btn500Dmg2P);
            btn1000Dmg2P = (Button) findViewById(R.id.btn1000Dmg2P);
            btnDivide2P = (Button)findViewById(R.id.btnDivide2P);
            btnOk1P = (Button)findViewById(R.id.btnOk1P);
            btnOk2P = (Button)findViewById(R.id.btnOk2P);
            btnClear1P = (Button)findViewById(R.id.btnClear1P);
            btnClear2P = (Button)findViewById(R.id.btnClear2P);
            btnVolverdmg1P = (Button)findViewById(R.id.btnBackDmgP1);
            btnVolverdmg2P = (Button)findViewById(R.id.btnBackDmgP2);
            btnHistory = (Button)findViewById(R.id.btnHistory);

            imgBtnDado = (ImageButton) findViewById(R.id.imgBtnDado);
            imgBtnFlip = (ImageButton) findViewById(R.id.imgBtnFlip);
            btnBoot1P = (ImageButton) findViewById(R.id.btnBoot1P);
            btnBoot2P = (ImageButton) findViewById(R.id.btnBoot2P);
            imgBtnSetting = (ImageButton) findViewById(R.id.imgBtnSetting2P);

            txtLifeCounter1P = (TextView) findViewById(R.id.txtLifeCounter1P);
            txtLifeCounter2P = (TextView) findViewById(R.id.txtLifeCounter2P);
            txtDamage1P = (TextView) findViewById(R.id.txtDamage1P);
            txtDamage2P = (TextView) findViewById(R.id.txtDamage2P);

            swtDamage1P = (Switch) findViewById(R.id.swtDamage1P);
            swtDamage2P = (Switch) findViewById(R.id.swtDamage2P);

            view1P =(View)findViewById(R.id.divider);




            btn50Dmg1P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SubirDamage1P(50);
                }
            });

            btn100Dmg1P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SubirDamage1P(100);
                }
            });

            btn500Dmg1P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SubirDamage1P(500);
                }
            });

            btn1000Dmg1P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SubirDamage1P(1000);
                }
            });

            btnDivide1P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!swtDamage1P.isChecked()){
                        SubirDamage1P(sLife1p/2);
                    }else{
                        SubirDamage1P(sLife1p);
                    }
                }
            });

            btn50Dmg2P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SubirDamage2P(50);
                }
            });

            btn100Dmg2P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SubirDamage2P(100);
                }
            });

            btn500Dmg2P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SubirDamage2P(500);
                }
            });

            btn1000Dmg2P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SubirDamage2P(1000);
                }
            });

            btnDivide2P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!swtDamage2P.isChecked()){
                        SubirDamage2P(sLife2p/2);
                    }else{
                        SubirDamage2P(sLife2p);
                    }
                }
            });

            swtDamage1P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (swtDamage1P.isChecked()) {
                        btn50Dmg1P.setBackgroundColor(Color.GREEN);
                        btn100Dmg1P.setBackgroundColor(Color.GREEN);
                        btn500Dmg1P.setBackgroundColor(Color.GREEN);
                        btn1000Dmg1P.setBackgroundColor(Color.GREEN);
                        btnDivide1P.setBackgroundColor(Color.GREEN);
                        btnDivide1P.setText("*2");
                    } else {
                        btn50Dmg1P.setBackgroundColor(Color.RED);
                        btn100Dmg1P.setBackgroundColor(Color.RED);
                        btn500Dmg1P.setBackgroundColor(Color.RED);
                        btn1000Dmg1P.setBackgroundColor(Color.RED);
                        btnDivide1P.setBackgroundColor(Color.RED);
                        btnDivide1P.setText("/2");
                    }
                }
            });

            swtDamage2P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (swtDamage2P.isChecked()) {
                        btn50Dmg2P.setBackgroundColor(Color.GREEN);
                        btn100Dmg2P.setBackgroundColor(Color.GREEN);
                        btn500Dmg2P.setBackgroundColor(Color.GREEN);
                        btn1000Dmg2P.setBackgroundColor(Color.GREEN);
                        btnDivide2P.setBackgroundColor(Color.GREEN);
                        btnDivide2P.setText("*2");
                    } else {
                        btn50Dmg2P.setBackgroundColor(Color.RED);
                        btn100Dmg2P.setBackgroundColor(Color.RED);
                        btn500Dmg2P.setBackgroundColor(Color.RED);
                        btn1000Dmg2P.setBackgroundColor(Color.RED);
                        btnDivide2P.setBackgroundColor(Color.RED);
                        btnDivide2P.setText("/2");
                    }
                }
            });

            btnOk1P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Damage1p == 0)
                        return;

                    final int RealDamage = sLife1p;
                    final int dmg = Damage1p;
                    stack1P.push(dmg*-1);
                    btnVolverdmg1P.setVisibility(View.VISIBLE);
                    sLife1p = RealDamage + dmg;
                    if(dmg>0)
                        History += ("Jugador 1: "+txtLifeCounter1P.getText()+"+"+txtDamage1P.getText()+"="+String.valueOf(sLife1p)+"\n");
                    else
                        History += ("Jugador 1: "+txtLifeCounter1P.getText()+txtDamage1P.getText()+"="+String.valueOf(sLife1p)+"\n");
                    txtDamage1P.setText("0");
                    Damage1p= 0;
                    txtDamage1P.setVisibility(View.INVISIBLE);
                    btnOk1P.setVisibility(View.INVISIBLE);
                    btnClear1P.setVisibility(View.INVISIBLE);
                    mOperations.startSound(Main2Activity.this,checked);

                    new CountDownTimer(2500, 250) {
                        public void onTick(long millisUntilFinished) {
                            txtLifeCounter1P.setText(String.valueOf(new Random().nextInt(8000)));
                        }

                        public void onFinish() {
                            if (sLife1p == 0) {
                                new AlertDialog.Builder(Main2Activity.this)
                                        .setMessage("Tus puntos de vida han quedado a cero\n" +
                                                "desea reiniciar?")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                txtLifeCounter1P.setText(Life);
                                                txtLifeCounter2P.setText(Life);
                                                sLife1p = Integer.parseInt(Life);
                                                sLife2p = Integer.parseInt(Life);
                                                stack1P.clear();
                                                stack2P.clear();
                                                btnVolverdmg1P.setVisibility(View.INVISIBLE);
                                                btnVolverdmg2P.setVisibility(View.INVISIBLE);
                                                History="";
                                            }
                                        }).setNegativeButton("NO", null).create().show();
                            }
                            txtLifeCounter1P.setText(String.valueOf(sLife1p));
                        }
                    }.start();
                }
            });

            btnOk2P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Damage2p == 0)
                        return;

                    final int RealDamage = sLife2p;
                    final int dmg = Damage2p;
                    stack2P.push(dmg*-1);
                    btnVolverdmg2P.setVisibility(View.VISIBLE);
                    sLife2p = RealDamage + dmg;
                    if(dmg>0)
                        History += ("Jugador 2: "+txtLifeCounter2P.getText()+"+"+txtDamage2P.getText()+"="+String.valueOf(sLife2p)+"\n");
                    else
                        History += ("Jugador 2: "+txtLifeCounter2P.getText()+txtDamage2P.getText()+"="+String.valueOf(sLife2p)+"\n");
                    Damage2p= 0;
                    txtDamage2P.setText("0");
                    txtDamage2P.setVisibility(View.INVISIBLE);
                    btnOk2P.setVisibility(View.INVISIBLE);
                    btnClear2P.setVisibility(View.INVISIBLE);

                    mOperations.startSound(Main2Activity.this,checked);

                    new CountDownTimer(2500, 250) {
                        public void onTick(long millisUntilFinished) {
                            txtLifeCounter2P.setText(String.valueOf(new Random().nextInt(8000)));
                        }

                        public void onFinish() {

                            int TotalLife = RealDamage + dmg;
                            if (TotalLife == 0) {
                                new AlertDialog.Builder(Main2Activity.this)
                                        .setMessage("Tus puntos de vida han quedado a cero\n" +
                                                "desea reiniciar?")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                txtLifeCounter2P.setText(Life);
                                                txtLifeCounter1P.setText(Life);
                                                sLife1p = Integer.parseInt(Life);
                                                sLife2p = Integer.parseInt(Life);
                                                stack1P.clear();
                                                stack2P.clear();
                                                btnVolverdmg1P.setVisibility(View.INVISIBLE);
                                                btnVolverdmg2P.setVisibility(View.INVISIBLE);
                                                History="";
                                            }
                                        }).setNegativeButton("NO", null).create().show();
                            }
                            txtLifeCounter2P.setText(String.valueOf(TotalLife));
                        }
                    }.start();
                }
            });

            btnVolverdmg1P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!stack1P.empty()){
                        int dmg = stack1P.pop();
                        sLife1p = sLife1p + dmg;
                        if(dmg>0)
                            History += ("Jugador1: "+txtLifeCounter1P.getText()+"+"+String.valueOf(dmg)+"="+String.valueOf(sLife1p)+"\n");
                        else
                            History += ("Jugador1: "+txtLifeCounter1P.getText()+String.valueOf(dmg)+"="+String.valueOf(sLife1p)+"\n");
                        txtLifeCounter1P.setText(String.valueOf(sLife1p));
                        if(stack1P.empty()){
                            btnVolverdmg1P.setVisibility(View.INVISIBLE);
                        }
                    }
                }
            });

            btnVolverdmg2P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!stack2P.empty()){
                        int dmg = stack2P.pop();
                        sLife2p = sLife2p + dmg;
                        if(dmg>0)
                            History += ("Jugador2: "+txtLifeCounter2P.getText()+"+"+String.valueOf(dmg)+"="+String.valueOf(sLife2p)+"\n");
                        else
                            History += ("Jugador2: "+txtLifeCounter2P.getText()+String.valueOf(dmg)+"="+String.valueOf(sLife2p)+"\n");
                        txtLifeCounter2P.setText(String.valueOf(sLife2p));
                        if(stack2P.empty()){
                            btnVolverdmg2P.setVisibility(View.INVISIBLE);
                        }
                    }
                }
            });

            btnClear1P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnClear1P.setVisibility(View.INVISIBLE);
                    btnOk1P.setVisibility(View.INVISIBLE);
                    txtDamage1P.setText("0");
                    Damage1p = 0;
                    txtDamage1P.setVisibility(View.INVISIBLE);
                }
            });

            btnClear2P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnClear2P.setVisibility(View.INVISIBLE);
                    btnOk2P.setVisibility(View.INVISIBLE);
                    txtDamage2P.setText("0");
                    Damage2p = 0;
                    txtDamage2P.setVisibility(View.INVISIBLE);
                }
            });

            btnBoot1P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtLifeCounter1P.setText(Life);
                    sLife1p = Integer.parseInt(Life);
                    stack1P.clear();
                    btnVolverdmg1P.setVisibility(View.INVISIBLE);
                    boot1P = true;
                    if(boot1P && boot2P){
                        History="";
                        boot1P = false;
                        boot2P=false;
                    }else {
                        History += "Jugador 1: Reinicio contador a " + Life +"\n";
                    }
                }
            });

            btnBoot2P.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtLifeCounter2P.setText(Life);
                    sLife2p = Integer.parseInt(Life);
                    stack2P.clear();
                    btnVolverdmg2P.setVisibility(View.INVISIBLE);
                    boot2P =true;
                    if(boot1P && boot2P){
                        History="";
                        boot1P = false;
                        boot2P=false;
                    }else {
                        History += "Jugador 2: Reinicio contador a " + Life + "\n";

                    }
                }
            });

            imgBtnDado.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(Main2Activity.this,
                            "El resultado del dado es " + String.valueOf(new Random().nextInt(6) + 1),
                            Toast.LENGTH_SHORT).show();
                }
            });

            imgBtnFlip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String moneda = mOperations.Flip();
                    Toast.makeText(Main2Activity.this,
                            ("El resultado de la moneda es " + moneda),
                            Toast.LENGTH_SHORT).show();
                }
            });

            imgBtnSetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Musica != null)
                        Musica.stop();
                    Intent i = new Intent(Main2Activity.this, config.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.putExtra("inicio", true);
                    startActivity(i);
                    finish();
                }
            });

        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(Main2Activity.this)
                        .setMessage(History)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {}
                        }).create().show();
            }
        });

        if (getIntent().getIntExtra("lp", 2) == 1) {
            Life = "4000";
        } else if (getIntent().getIntExtra("lp", 2) == 2) {
            Life = "8000";
        } else {
            Life = "16000";
        }
        txtLifeCounter2P.setText(Life);
        txtLifeCounter1P.setText(Life);
        sLife1p = Integer.parseInt(Life);
        sLife2p = sLife1p;

        checked = getIntent().getBooleanExtra("sound",true);

       if(getIntent().getBooleanExtra("musica",true)){
            Musica = MediaPlayer.create(Main2Activity.this,R.raw.duel);
            Musica.start();
            Musica.setLooping(true);
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onStart() {
        super.onStart();
        view1P.setBackgroundResource(getIntent().getIntExtra("images",R.color.colorGray));

    }

    private  void SubirDamage1P(int btnDamage){
        int Life = sLife1p;
        Damage1p = mOperations.damage(btnDamage,Damage1p,swtDamage1P.isChecked(),Life);
        if(!swtDamage1P.isChecked()) {
            txtDamage1P.setText(String.valueOf(Damage1p));
            txtDamage1P.setTextColor(Color.RED);
        }else{
            txtDamage1P.setText(String.valueOf(Damage1p));
            if(Damage1p ==0 )
                txtDamage1P.setTextColor(Color.WHITE);
            else if(Damage1p >0){
                txtDamage1P.setTextColor((Color.GREEN));
            }
        }
        txtDamage1P.setVisibility(View.VISIBLE);
        btnClear1P.setVisibility(View.VISIBLE);
        btnOk1P.setVisibility(View.VISIBLE);
    }

    private  void SubirDamage2P(int btnDamage){
        int Life = sLife2p;
        Damage2p= mOperations.damage(btnDamage,Damage2p,swtDamage2P.isChecked(),Life);
        if(!swtDamage2P.isChecked()) {
            txtDamage2P.setText(String.valueOf(Damage2p));
            txtDamage2P.setTextColor(Color.RED);
        }else{
            txtDamage2P.setText(String.valueOf(Damage2p));
            if(Damage2p==0)
                txtDamage2P.setTextColor(Color.WHITE);
            else if(Damage2p>0){
                txtDamage2P.setTextColor(Color.GREEN);
            }
        }
        txtDamage2P.setVisibility(View.VISIBLE);
        btnClear2P.setVisibility(View.VISIBLE);
        btnOk2P.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if(Musica != null)
            Musica.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Musica != null)
            Musica.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
        System.exit(0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();

    }
}
