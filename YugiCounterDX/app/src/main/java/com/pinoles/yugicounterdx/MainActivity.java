package com.pinoles.yugicounterdx;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.Stack;

public class MainActivity extends AppCompatActivity {

    ConstraintLayout Main1Layout;

    TextView txtLife, txtDamage;
    Button btn50,btn100,btn500,btn1000,btnOk,btnClear,btnDivide, btnVolverDmg,btnHistory;
    ImageButton btnReboot,btnDado,btnFlip,btnSetting;
    Switch swtDmg;

    CounterOperation mOperations = new CounterOperation();

    MediaPlayer lpSound, Musica;
    boolean checked ;
    int Damage, sLife;
    String LifeConfi,History="";

    Stack<Integer> stack1 = new Stack<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Damage = 0;

        lpSound = MediaPlayer.create(this,R.raw.lp_sound);

        txtLife = (TextView)findViewById(R.id.txtLifeCounter);
        txtDamage = (TextView)findViewById(R.id.txtDamageCounter);

        btn50 = (Button)findViewById(R.id.btn50dmg);
        btn100 = (Button)findViewById(R.id.btn100dmg);
        btn500 = (Button)findViewById(R.id.btn500dmg);
        btn1000 = (Button)findViewById(R.id.btn1000dmg);
        btnVolverDmg = (Button)findViewById(R.id.btnbackdmg);
        btnDivide = (Button)findViewById(R.id.btnDivide);
        btnOk = (Button)findViewById(R.id.btnOkDmg);
        btnClear = (Button)findViewById(R.id.btnClearDmg);
        btnHistory=(Button)findViewById(R.id.btnHistory);
        btnReboot = (ImageButton)findViewById(R.id.btnReboot);
        btnDado = (ImageButton)findViewById(R.id.btnDado);
        btnFlip = (ImageButton)findViewById(R.id.btnFlip);
        btnSetting = (ImageButton)findViewById(R.id.imgBtnSetting);
        swtDmg=(Switch)findViewById(R.id.swtDamage);


        Main1Layout = (ConstraintLayout)findViewById(R.id.Main1Layout);

        Main1Layout.setBackgroundResource(getIntent().getIntExtra("images",R.color.colorGray));

        if(getIntent().getIntExtra("lp",2)==1){
            txtLife.setText("4000");
            LifeConfi = "4000";
            sLife = 4000;
        }else if(getIntent().getIntExtra("lp",2)==2){
            txtLife.setText("8000");
            LifeConfi = "8000";
            sLife = 8000;
        }else{
            txtLife.setText("16000");
            sLife = 16000;
            LifeConfi = "16000";
        }

        checked = getIntent().getBooleanExtra("sound",true);

        if(getIntent().getBooleanExtra("musica",true)){
            Musica = MediaPlayer.create(MainActivity.this,R.raw.duel);
            Musica.start();
            Musica.setLooping(true);
        }

        btn50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubirDamage(50);
            }
        });

        btn100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    SubirDamage(100);
            }
        });

        btn500.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    SubirDamage(500);
        }});

        btn1000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    SubirDamage(1000);
            }
        });

        btnDivide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!swtDmg.isChecked()){
                    SubirDamage(sLife/2);
                }else{
                    SubirDamage(sLife);
                }
            }
        });

        btnReboot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtLife.setText(LifeConfi);
                sLife  = Integer.parseInt(LifeConfi);
                stack1.clear();
                btnVolverDmg.setVisibility(View.INVISIBLE);
            }
        });

        btnVolverDmg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!stack1.empty()){
                    int dmg = stack1.pop();
                    sLife = sLife + dmg;
                    if(dmg>0)
                        History += ("Jugador1: "+txtLife.getText()+"+"+String.valueOf(dmg)+"="+String.valueOf(sLife)+"\n");
                    else
                        History += ("Jugador1: "+txtLife.getText()+String.valueOf(dmg)+"="+String.valueOf(sLife)+"\n");
                    txtLife.setText(String.valueOf(sLife));
                    if(stack1.empty()){
                        btnVolverDmg.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtDamage.getText().toString().equals("0"))
                    return;

                final int  RealDamage = sLife;
                final int dmg = Damage;
                stack1.push(dmg*-1);
                sLife =  RealDamage + dmg;
                if(Damage>0)
                    History += ("Jugador 1: "+txtLife.getText()+"+"+txtDamage.getText()+"="+String.valueOf(sLife)+"\n");
                else
                    History += ("Jugador 1: "+txtLife.getText()+txtDamage.getText()+"="+String.valueOf(sLife)+"\n");
                Damage  = 0;
                txtDamage.setText("0");
                txtDamage.setVisibility(View.INVISIBLE);
                btnVolverDmg.setVisibility(View.VISIBLE);
                mOperations.startSound(MainActivity.this,checked);

                new CountDownTimer(2500, 250) {
                    public void onTick(long millisUntilFinished) {
                        txtLife.setText(String.valueOf(new Random().nextInt(8000)));
                    }

                    public void onFinish() {
                        if(sLife == 0){
                            new AlertDialog.Builder(MainActivity.this)
                                    .setMessage("Tus puntos de vida han quedado a cero\n" +
                                            "desea reiniciar?")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            txtLife.setText(LifeConfi);
                                            sLife = Integer.parseInt(LifeConfi);
                                            stack1.clear();
                                            btnVolverDmg.setVisibility(View.INVISIBLE);
                                            History ="";
                                        }
                                    }).setNegativeButton("NO",null).create().show();
                        }
                        txtLife.setText(String.valueOf(sLife));
                    }
                }.start();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtDamage.setText("0");
                Damage = 0;
                txtDamage.setVisibility(View.INVISIBLE);
            }
        });

        btnDado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,
                        "El resultado del dado es " + String.valueOf(new Random().nextInt(6)+1),
                        Toast.LENGTH_SHORT).show();
            }
        });

        btnFlip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String moneda = mOperations.Flip();
                Toast.makeText(MainActivity.this,
                        ("El resultado de la moneda es " + moneda),
                        Toast.LENGTH_SHORT).show();
            }
        });

        swtDmg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(swtDmg.isChecked()){
                    btn50.setBackgroundColor(Color.GREEN);
                    btn100.setBackgroundColor(Color.GREEN);
                    btn500.setBackgroundColor(Color.GREEN);
                    btn1000.setBackgroundColor(Color.GREEN);
                    btnDivide.setBackgroundColor(Color.GREEN);
                    btnDivide.setText("*2");
                }else{
                    btn50.setBackgroundColor(Color.RED);
                    btn100.setBackgroundColor(Color.RED);
                    btn500.setBackgroundColor(Color.RED);
                    btn1000.setBackgroundColor(Color.RED);
                    btnDivide.setBackgroundColor(Color.RED);
                    btnDivide.setText("/2");
                }
            }
        });

        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Musica != null)
                    Musica.stop();
                Intent i = new Intent(MainActivity.this,config.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("inicio",true);
                startActivity(i);
                finish();
            }
        });

        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage(History)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {}
                        }).create().show();
            }
        });
    }

    private  void SubirDamage(int btnDamage){
        int Life = sLife;
        Damage = mOperations.damage(btnDamage,Damage,swtDmg.isChecked(),Life);
        if(!swtDmg.isChecked()) {
            txtDamage.setText(String.valueOf(Damage));
            txtDamage.setVisibility(View.VISIBLE); if(Damage == 0){
                txtDamage.setTextColor(Color.WHITE);
            }else if(Damage<0)
                txtDamage.setTextColor(Color.RED);
        }else{
            txtDamage.setText(String.valueOf(Damage));
            txtDamage.setVisibility(View.VISIBLE);
            if(Damage == 0){
                txtDamage.setTextColor(Color.WHITE);
            }else if(Damage>0)
                txtDamage.setTextColor(Color.GREEN);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finishAffinity();
        System.exit(0);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if(Musica != null)
            Musica.pause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Musica != null)
        Musica.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finishAffinity();

    }
}
