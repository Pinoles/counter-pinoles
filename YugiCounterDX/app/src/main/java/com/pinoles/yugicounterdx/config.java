package com.pinoles.yugicounterdx;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ToggleButton;

public class config extends AppCompatActivity {

    int[] imagenes;
    int imagenSelected,puntador;

    RadioGroup rdGrpLife,rdGrpPlayers;
    RadioButton rdBtn4k,rdBtn8k,rdBtn16k,rdBtn1P,rdBtn2P,rdBtn4P;

    ToggleButton tglBtnSound,tglBtnMusica;

    Button btnOkConfig,btnIzquierda,btnDerecha;

    ImageView imgVwFondo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Context mContex = this;
        SharedPreferences bootsharpref = getPreferences(mContex.MODE_PRIVATE);
        int intValueRdGrpLife = bootsharpref.getInt("Life",2);
        int intValueRdGrpPlayers = bootsharpref.getInt("Players",1);
        Boolean boolValueTglBtnSound = bootsharpref.getBoolean("Sound",true);
        Boolean boolValueTglBtnMusica = bootsharpref.getBoolean("Musica",true);
        int intValueImageSelected = bootsharpref.getInt("Images",R.color.colorGray);
        imagenSelected = intValueImageSelected;

        if(!getIntent().getBooleanExtra("inicio",false)){
            Intent i;
            switch (intValueRdGrpPlayers) {
                case 1:
                    i = new Intent(config.this, MainActivity.class);
                    break;
                case 2:
                    i = new Intent(config.this, Main2Activity.class);
                    break;
                default:
                    i = new Intent(config.this, FourPlayersActivity.class);
                    break;
            }
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra("lp", intValueRdGrpLife);
            i.putExtra("sound",boolValueTglBtnSound);
            i.putExtra("images",intValueImageSelected);
            i.putExtra("musica",boolValueTglBtnMusica);
            startActivity(i);
            finish();
        }

        puntador = 0;

        setContentView(R.layout.activity_config);

        rdGrpLife = (RadioGroup)findViewById(R.id.rdGrpLife);
        rdGrpPlayers = (RadioGroup)findViewById(R.id.rdGrpPlayers);

        rdBtn4k = (RadioButton)findViewById(R.id.rdBtn4k);
        rdBtn8k = (RadioButton)findViewById(R.id.rdBtn8k);
        rdBtn16k = (RadioButton)findViewById(R.id.rdBtn16k);
        rdBtn1P = (RadioButton)findViewById(R.id.rdBtn1P);
        rdBtn2P = (RadioButton)findViewById(R.id.rdBtn2P);
        rdBtn4P = (RadioButton) findViewById(R.id.rdBtn4P);

        tglBtnSound = (ToggleButton)findViewById(R.id.tglBtnSound);
        tglBtnMusica = (ToggleButton)findViewById(R.id.tglBtnMusica);

        btnOkConfig = (Button)findViewById(R.id.btnOkConfig);
        btnDerecha = (Button)findViewById(R.id.btnDerecha);
        btnIzquierda = (Button)findViewById(R.id.btnIzquierda);

        imgVwFondo = (ImageView)findViewById(R.id.imgVwFondo);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        switch (intValueRdGrpLife){
            case  1:
                rdBtn4k.setChecked(false);
                rdBtn8k.setChecked(false);
                rdBtn16k.setChecked(false);
                rdBtn4k.setChecked(true);
                break;
            case 2:
                rdBtn4k.setChecked(false);
                rdBtn8k.setChecked(false);
                rdBtn16k.setChecked(false);
                rdBtn8k.setChecked(true);
                break;
            case 3:
                rdBtn4k.setChecked(false);
                rdBtn8k.setChecked(false);
                rdBtn16k.setChecked(false);
                rdBtn16k.setChecked(true);
                break;
        }

        if(intValueRdGrpPlayers==1){
            rdBtn2P.setChecked(false);
            rdBtn1P.setChecked(true);
            rdBtn4P.setChecked(false);
        }else if(intValueRdGrpPlayers==2){
            rdBtn2P.setChecked(true);
            rdBtn1P.setChecked(false);
            rdBtn4P.setChecked(false);
        }else{
            rdBtn2P.setChecked(false);
            rdBtn1P.setChecked(false);
            rdBtn4P.setChecked(true);
        }

        tglBtnSound.setChecked(boolValueTglBtnSound);
        tglBtnMusica.setChecked(boolValueTglBtnMusica);

        imagenes = new int[] {R.color.colorGray,R.drawable.blue_eyes, R.drawable.red_eyes, R.drawable.dark_magician,
                R.drawable.elemental_neos, R.drawable.dr_crowler, R.drawable.salamangreat, R.drawable.max_dragon};

        imgVwFondo.setBackground(getResources().getDrawable(intValueImageSelected));

        btnIzquierda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(puntador==0){
                    puntador = imagenes.length-1;
                }else{
                    puntador --;
                }
                imgVwFondo.setBackgroundResource(imagenes[puntador]);
                imagenSelected = imagenes[puntador];
            }
        });

        btnDerecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(puntador==(imagenes.length-1)){
                    puntador = 0;
                }else{
                    puntador ++;
                }
                imgVwFondo.setBackgroundResource(imagenes[puntador]);
                imagenSelected = imagenes[puntador];
            }
        });

        btnOkConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int intValueLife,intValuePlayers;

                SharedPreferences sharpref = getPreferences(mContex.MODE_PRIVATE);
                SharedPreferences.Editor mEditor = sharpref.edit();
                if(rdBtn4k.isChecked()){
                    mEditor.putInt("Life",1);
                    intValueLife =1;
                }else if (rdBtn8k.isChecked()){
                    mEditor.putInt("Life",2);
                    intValueLife =2;
                }else{
                    mEditor.putInt("Life",3);
                    intValueLife =3;
                }

                if(rdBtn1P.isChecked()){
                    mEditor.putInt("Players",1);
                    intValuePlayers =1;
                }else if (rdBtn2P.isChecked()){
                    mEditor.putInt("Players",2);
                    intValuePlayers =2;
                }else{
                    mEditor.putInt("Players",3);
                    intValuePlayers =3;
                }
                mEditor.putBoolean("Sound",tglBtnSound.isChecked());
                mEditor.putBoolean("Musica",tglBtnMusica.isChecked());

                mEditor.putInt("Images",imagenSelected);

                mEditor.commit();

                Intent i;
                if(intValuePlayers==1) {
                    i = new Intent(config.this, MainActivity.class);
                }else if (intValuePlayers==2){
                    i = new Intent(config.this, Main2Activity.class);
                }else{
                    i = new Intent(config.this, FourPlayersActivity.class);
                }
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("lp", intValueLife);
                i.putExtra("sound",tglBtnSound.isChecked());
                i.putExtra("musica",tglBtnMusica.isChecked());
                i.putExtra("images",imagenSelected);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
